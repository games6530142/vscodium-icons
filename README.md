```json
  /* #region material icons */
  "material-icon-theme.files.associations": {
    ".eslintrc.base.js": "eslint",
    // custom
    "*.controller.ts": "../../icons/icons/controller/controller",
    "*.controllers.ts": "../../icons/icons/controller/controller",
    "*.module.ts": "../../icons/icons/module/module",
    "*.modules.ts": "../../icons/icons/module/module",
    "*.service.ts": "../../icons/icons/service/service",
    "*.services.ts": "../../icons/icons/service/service"
  },
  "material-icon-theme.folders.associations": {
    "controller": "../../../../icons/icons/controller/folder-controller",
    "controllers": "../../../../icons/icons/controller/folder-controller",
    "module": "../../../../icons/icons/module/folder-module",
    "modules": "../../../../icons/icons/module/folder-module",
    "^service[s]?$": "../../../../icons/icons/service/folder-service",
    "services": "../../../../icons/icons/service/folder-service"
  },
  "material-icon-theme.activeIconPack": "none",
  "material-icon-theme.folders.theme": "classic",
  "material-icon-theme.hidesExplorerArrows": false,
  /* #endregion */
```